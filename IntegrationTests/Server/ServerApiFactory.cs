﻿using Infrastructure.Db;
using IntegrationTests.Db;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using WebApiTestTask;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace IntegrationTests.Server
{
    internal class ServerApiFactory : WebApplicationFactory<Startup>
    {

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder
                .UseEnvironment("Development")
                .ConfigureAppConfiguration((context, builder) =>
                {

                })
                .ConfigureServices(services =>
                {

                });
        }

        public IDbExecuter GetDbExecuter()
        {
            var dbConectionFactory = Services.GetRequiredService<IDatabaseConnectionFactory>();
            var msExecuter = new MsDbExecuter(dbConectionFactory.GetConnectionString());
            return msExecuter;
        }

        public async Task<CustomerAgent> CreateAdminAuthHttpClient()
        {
            var customerAdmin = new CustomerCreateRequest() 
            { 
                Name = "AdminTest1", 
                Password = "TestPassword" 
            };
            var customerAgent = await CreateAuthHttpClient(customerAdmin, true);
            return customerAgent;
        }

        public async Task<CustomerAgent> CreateAuthHttpClient()
        {
            var customerAdmin = new CustomerCreateRequest()
            {
                Name = "Test1",
                Password = "TestPassword"
            };
            var customerAgent = await CreateAuthHttpClient(customerAdmin, false);
            return customerAgent;
        }

        public async Task<CustomerAgent> CreateAuthHttpClient(CustomerCreateRequest customerToCreate, bool isAdmin = false)
        {
            var httpClinet = CreateClient();
            var createdCustomer = await CreateCustomer(httpClinet, customerToCreate);
            if (isAdmin)
            {
                await SetUserAdmin(createdCustomer.Number);
            }
            await LogIn(httpClinet, createdCustomer.Number, customerToCreate.Password);
            var customerAgent = new CustomerAgent()
            {
                HttpClient = httpClinet,
                CustomerNumber = createdCustomer.Number,
            };
            return customerAgent;
        }

        private async Task<bool> SetUserAdmin(int customerNumber)
        {
            var query = "INSERT INTO [dbo].[CustomerRoles] " +
                "([CustomerId] ,[RoleId]) " +
                $"select {customerNumber}, r.Id  " +
                "from Roles r " +
                "where r.Name like 'Admin' ";
            var rows = await GetDbExecuter().Execute(query);
            return rows > 0;
        }

        private static async Task LogIn(HttpClient httpCliet, int customerNumber, string pwd)
        {
            var logModel = new CustomerLoginRequest()
            {
                Number = customerNumber,
                Password = pwd
            };
            var logInResponce = await httpCliet.PostAsJsonAsync("Token/login", logModel);
            if (!logInResponce.IsSuccessStatusCode)
            {
                var msg = await logInResponce.Content.ReadAsStringAsync();
                Assert.Fail($"Could not login: Response code:{logInResponce.StatusCode}, msg:{msg ?? "empty"}");
            }
            var logCred = await logInResponce.Content.ReadFromJsonAsync<TokenResponse>();
            Assert.IsNotNull(logCred);
            httpCliet.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", logCred.Token);
        }

        private async Task<CustomerResponse> CreateCustomer(HttpClient httpClient, CustomerCreateRequest customer)
        {
            var createCustomerRespone = await httpClient.PostAsJsonAsync($"Customers/create", customer);

            Assert.IsTrue(createCustomerRespone.IsSuccessStatusCode, "Failed to create customer");
            var createdCustomer = await createCustomerRespone.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(createdCustomer, "Response can't be null");
            return createdCustomer;
        }

        public class CustomerAgent
        {
            public HttpClient HttpClient { get; set; }
            public int CustomerNumber { get; set; }
        }
    }
}

﻿using Domain.Contracts.Db.Repositories;
using Domain.Exceptions;
using Domain.Models.Entities;
using IntegrationTests.Db;
using IntegrationTests.Server;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTests.DbTest
{
    [TestClass]
    public class DbConcurrencyTest
    {
        private readonly ServerApiFactory _factory = new();
        private IDbExecuter _dbExecuter = null;

        [TestInitialize]
        public void Init()
        {
            _dbExecuter = _factory.GetDbExecuter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbExecuter.Execute("delete from [dbo].[Customers]");
        }

        [TestMethod]
        public async Task Update_InsertDataTryToModificate_Error()
        {
            
            using IServiceScope scope = _factory.Services.CreateScope();
            var customerRepo = scope.ServiceProvider.GetService<ICustomerRepository>();
            var customer = new CustomerEntity()
            {
                Name = "Test",
                Password = "as"
            };
            customerRepo.Insert(customer);
            await customerRepo.SaveAsync();
            var testCustomer1 = await customerRepo.FindAsync(customer.Number);
            await _dbExecuter.Execute($"UPDATE [dbo].[Customers] SET Name = 'Test23232' " +
                $" WHERE Number ={testCustomer1.Number}");
            testCustomer1.Name = "Test1";
            customerRepo.Update(testCustomer1);
                
            await Assert.ThrowsExceptionAsync<DbConcurrencyException>(
                    () => customerRepo.SaveAsync()
                );
        }
    }
}

﻿using Domain.Models.Entities;
using IntegrationTests.Db;
using IntegrationTests.Server;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http.Json;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace IntegrationTests.ControllersTest
{
    [TestClass]
    public class EmployeeControllerTest
    {
        private readonly string _baseUri = "Employee";
        private readonly ServerApiFactory _factory = new();
        private HttpClient _httpClient = null;
        private IDbExecuter _dbExecuter = null;
        

        [TestInitialize]
        public void Init()
        {   
            _httpClient = _factory.CreateClient();
            _dbExecuter = _factory.GetDbExecuter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbExecuter.Execute("delete from [dbo].[Customers]");
            _dbExecuter.Execute("delete from [dbo].[Employee]");
        }

        [TestMethod]
        public async Task Create_NotAuthCreateEmployee_GetUnauthorized()
        {
            var employee = new EmployeeCreateRequest
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "Email@test.com",
                PhoneNumber = "1234567890",
            };
            var response = await _httpClient.PostAsJsonAsync($"{_baseUri}/create", employee);
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Create_CreateEmployee_GetOk()
        {
            var employee = new EmployeeCreateRequest
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "Email@test.com",
                PhoneNumber = "1234567890",
            };
            var agent = await _factory.CreateAuthHttpClient();
            var createdEmployee = await CreateEmployee(employee, agent.HttpClient);
            Assert.IsTrue(createdEmployee.Id > 0);
            var dbEmployee = await GetEmployeeFromDbAsync(createdEmployee.Id);
            Assert.IsNotNull(dbEmployee);
            Assert.AreEqual(dbEmployee.CustomerNumber, agent.CustomerNumber);
        }

        [TestMethod]
        public async Task Get_NotAuthCreateEmployee_GetUnauthorized()
        {   
            var response = await _httpClient.GetAsync($"{_baseUri}");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Get_CreateEmployeeGetByOtheCustomer_GetNoEmployee()
        {   
            var agent1 = await _factory.CreateAuthHttpClient();
            var employee = await CreateEmployee(agent1.HttpClient);
            var agent2 = await _factory.CreateAuthHttpClient();

            var response = await agent2.HttpClient.GetAsync($"{_baseUri}");
            Assert.IsTrue(response.IsSuccessStatusCode);
            var employees = await response.Content.ReadFromJsonAsync<IEnumerable<EmployeeResponse>>();
            
            Assert.IsNotNull(employees);
            Assert.IsFalse(employees.Any());

            var dbEmployee = await GetEmployeeFromDbAsync(employee.Id);
            Assert.IsNotNull(dbEmployee);
        }

        [TestMethod]
        public async Task Get_CreateEmployee_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var employee = await CreateEmployee(agent.HttpClient);

            var response = await agent.HttpClient.GetAsync($"{_baseUri}");
            Assert.IsTrue(response.IsSuccessStatusCode);
            var employees = await response.Content.ReadFromJsonAsync<IEnumerable<EmployeeResponse>>();

            Assert.IsNotNull(employees);
            Assert.IsTrue(employees.Any());

            var dbEmployee = await GetEmployeeFromDbAsync(employee.Id);
            Assert.IsNotNull(dbEmployee);
        }

        [TestMethod]
        public async Task Update_NotAuth_GetUnauthorized()
        {
            var response = await _httpClient
                .PutAsJsonAsync($"{_baseUri}/update/0", new EmployeeUpdateRequest());
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Update_CreateEmployeeGetByOtheCustomer_GetNotFound()
        {
            var agent1 = await _factory.CreateAuthHttpClient();
            var employee = await CreateEmployee(agent1.HttpClient);
            var agent2 = await _factory.CreateAuthHttpClient();
            var update = new EmployeeUpdateRequest()
            {
                FirstName = "updateFirstName",
                LastName = "updateLastName",
                Email = "updateEmail@test.com",
                PhoneNumber = "update",
            };
            var response = await agent2.HttpClient
                .PutAsJsonAsync($"{_baseUri}/update/{employee.Id}", update);
            Assert.IsFalse(response.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public async Task Update_CreateEmployee_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var employee = await CreateEmployee(agent.HttpClient);
            var update = new EmployeeUpdateRequest()
            {
                FirstName = "updateFirstName",
                LastName = "updateLastName",
                Email = "updateEmail@test.com",
                PhoneNumber = "update",
            };
            var response = await agent.HttpClient
                .PutAsJsonAsync($"{_baseUri}/update/{employee.Id}", update);
            Assert.IsTrue(response.IsSuccessStatusCode);
            var updatedResponse = await response.Content.ReadFromJsonAsync<EmployeeResponse>();
            var dbEmployee = await GetEmployeeFromDbAsync(employee.Id);

            Assert.IsNotNull(updatedResponse);
            Assert.AreEqual(employee.Id, updatedResponse.Id);
            Assert.AreNotEqual(employee.FirstName, updatedResponse.FirstName);
            Assert.AreEqual(dbEmployee.Id, updatedResponse.Id);
            Assert.AreEqual(dbEmployee.FirstName, updatedResponse.FirstName);
        }

        private async Task<EmployeeResponse> CreateEmployee(HttpClient httpClient)
        {
            var employee = new EmployeeCreateRequest
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "Email@test.com",
                PhoneNumber = "1234567890",
            };
            var createdEmployee = await CreateEmployee(employee, httpClient);
            return createdEmployee;
        }

        private async Task<EmployeeResponse> CreateEmployee
            (EmployeeCreateRequest employee, HttpClient httpClient)
        {
            var response = await httpClient.PostAsJsonAsync($"{_baseUri}/create", employee);
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to create employee");

            var createdEmployee = await response.Content.ReadFromJsonAsync<EmployeeResponse>();
            Assert.IsNotNull(createdEmployee);
            return createdEmployee;
        }

        private async Task<EmployeeEntity> GetEmployeeFromDbAsync(int employyeId)
        {
            var employye = await _dbExecuter.Query<EmployeeEntity>($"SELECT * FROM [dbo].[Employee] s WHERE s.Id = {employyeId}");
            return employye.FirstOrDefault();
        }
    }
}

﻿using IntegrationTests.Db;
using IntegrationTests.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;
using Domain.Models.Entities;

namespace IntegrationTests.ControllersTest
{
    [TestClass]
    public class BusinessLocationControllerTest
    {
        private readonly string _baseUri = "BusinessLocation";
        private readonly string _employeeUri = "Employee";
        private readonly ServerApiFactory _factory = new();
        private HttpClient _httpClient = null;
        private IDbExecuter _dbExecuter = null;

        [TestInitialize]
        public void Init()
        {
            _httpClient = _factory.CreateClient();
            _dbExecuter = _factory.GetDbExecuter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbExecuter.Execute("delete from [dbo].[EmployeeBusinessLocation]");
            _dbExecuter.Execute("delete from [dbo].[Employee]");
            _dbExecuter.Execute("delete from [dbo].[BusinessLocations]");
            _dbExecuter.Execute("delete from [dbo].[Customers]");
        }

        [TestMethod]
        public async Task Create_NotAuth_GetUnauthorized()
        {
            var location = new BusinessLocationCreateRequest
            {
                Name = "TestFirstName",
                Address = "TestLastName",
                PhoneNumber = "1234567890",
            };
            var response = await _httpClient.PostAsJsonAsync($"{_baseUri}/create", location);
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Create_CreateLocation_GetOk()
        {
            var location = new BusinessLocationCreateRequest
            {
                Name = "TestFirstName",
                Address = "TestLastName",
                PhoneNumber = "1234567890",
            };
            var agent = await _factory.CreateAuthHttpClient();
            var createdLocation = await CreateLocation(location, agent.HttpClient);
            Assert.IsTrue(createdLocation.Id > 0);
            var dbLocation = await GetLocationFromDbAsync(createdLocation.Id);
            Assert.IsNotNull(dbLocation);
            Assert.AreEqual(dbLocation.CustomerNumber, agent.CustomerNumber);
        }

        [TestMethod]
        public async Task Get_NotAuth_GetUnauthorized()
        {
            var response = await _httpClient.GetAsync($"{_baseUri}");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Get_CreateLocationGetByOtheCustomer_GetNoLocations()
        {
            var agent1 = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent1.HttpClient);
            var agent2 = await _factory.CreateAuthHttpClient();

            var response = await agent2.HttpClient.GetAsync($"{_baseUri}");
            Assert.IsTrue(response.IsSuccessStatusCode);
            var locations = await response.Content.ReadFromJsonAsync<IEnumerable<BusinessLocationEmployeeResponse>>();

            Assert.IsNotNull(locations);
            Assert.IsFalse(locations.Any());

            var dbLocation = await GetLocationFromDbAsync(location.Id);
            Assert.IsNotNull(dbLocation);
        }

        [TestMethod]
        public async Task Get_CreateLocation_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent.HttpClient);

            var response = await agent.HttpClient.GetAsync($"{_baseUri}");
            Assert.IsTrue(response.IsSuccessStatusCode);
            var locations = await response.Content.ReadFromJsonAsync<IEnumerable<BusinessLocationEmployeeResponse>>();

            Assert.IsNotNull(locations);
            Assert.IsTrue(locations.Any());

            var dbLocation = await GetLocationFromDbAsync(location.Id);
            Assert.IsNotNull(dbLocation);
        }

        [TestMethod]
        public async Task Update_NotAuth_GetUnauthorized()
        {
            var response = await _httpClient
                .PutAsJsonAsync($"{_baseUri}/update/0", new BusinessLocationUpdateRequest());
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Update_CreateLocationGetByOtheCustomer_GetNotFound()
        {
            var agent1 = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent1.HttpClient);
            var agent2 = await _factory.CreateAuthHttpClient();
            var update = new BusinessLocationCreateRequest
            {
                Name = "TestFirstName",
                Address = "TestLastName",
                PhoneNumber = "1234567890",
            };
            var response = await agent2.HttpClient
                .PutAsJsonAsync($"{_baseUri}/update/{location.Id}", update);
            Assert.IsFalse(response.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public async Task Update_CreateEmployee_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent.HttpClient);
            var update = new BusinessLocationCreateRequest
            {
                Name = "Upated",
                Address = "Upated",
                PhoneNumber = "1234567890",
            };
            var response = await agent.HttpClient
                .PutAsJsonAsync($"{_baseUri}/update/{location.Id}", update);
            Assert.IsTrue(response.IsSuccessStatusCode);
            var updatedResponse = await response.Content.ReadFromJsonAsync<BusinessLocationResponse>();
            var dbLocation = await GetLocationFromDbAsync(location.Id);

            Assert.IsNotNull(updatedResponse);
            Assert.AreEqual(location.Id, updatedResponse.Id);
            Assert.AreNotEqual(location.Name, updatedResponse.Name);
            Assert.AreEqual(dbLocation.Id, updatedResponse.Id);
            Assert.AreEqual(dbLocation.Name, updatedResponse.Name);
        }

        [TestMethod]
        public async Task AddEmployee_NotAuth_GetUnauthorized()
        {
            var response = await _httpClient
                .PostAsJsonAsync($"{_baseUri}/add/employee", new AddLocationEmployeeRequest());
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task AddEmployee_CreateLocationAndEmployee_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent.HttpClient);
            var employee = await CreateEmployee(agent.HttpClient);
            var request = new AddLocationEmployeeRequest()
            {
                BusinessLocationId = location.Id,
                EmployeeId = employee.Id
            };
            var response = await agent.HttpClient
                .PostAsJsonAsync($"{_baseUri}/add/employee", request);
            Assert.IsTrue(response.IsSuccessStatusCode);

            var locationEmployee = await response.Content
                .ReadFromJsonAsync<BusinessLocationEmployeeResponse>();
            Assert.IsNotNull(locationEmployee);
            Assert.AreEqual(location.Id, locationEmployee.Id);
            Assert.IsTrue(locationEmployee.Employees.Any(x=>x.Id == employee.Id));
        }

        [TestMethod]
        public async Task AddEmployee_CreateLocationAndEmployeeByOtheCustomer_GetBadRequest()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var agent2 = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent.HttpClient);
            var employee = await CreateEmployee(agent2.HttpClient);
            var request = new AddLocationEmployeeRequest()
            {
                BusinessLocationId = location.Id,
                EmployeeId = employee.Id
            };
            var response = await agent.HttpClient
                .PostAsJsonAsync($"{_baseUri}/add/employee", request);
            Assert.IsFalse(response.IsSuccessStatusCode);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public async Task RemoveEmployee_NotAuth_GetUnauthorized()
        {
            var response = await _httpClient
                .PostAsJsonAsync($"{_baseUri}/0/remove/employee/0", "");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task RemoveEmployee_AddEmployeeToLocation_GetOk()
        {
            var agent = await _factory.CreateAuthHttpClient();
            var location = await CreateLocation(agent.HttpClient);
            var employee = await CreateEmployee(agent.HttpClient);
            var request = new AddLocationEmployeeRequest()
            {
                BusinessLocationId = location.Id,
                EmployeeId = employee.Id
            };
            var responseFromAdd = await agent.HttpClient
                .PostAsJsonAsync($"{_baseUri}/add/employee", request);
            Assert.IsTrue(responseFromAdd.IsSuccessStatusCode);

            var locationEmployee = await responseFromAdd.Content
                .ReadFromJsonAsync<BusinessLocationEmployeeResponse>();
            Assert.IsNotNull(locationEmployee);
            Assert.AreEqual(location.Id, locationEmployee.Id);
            Assert.IsTrue(locationEmployee.Employees.Any(x => x.Id == employee.Id));

            var responseFromRemove = await agent.HttpClient
                .PostAsJsonAsync($"{_baseUri}/{location.Id}/remove/employee/{employee.Id}", "");
            Assert.IsTrue(responseFromRemove.IsSuccessStatusCode);

            locationEmployee = await responseFromRemove.Content
                .ReadFromJsonAsync<BusinessLocationEmployeeResponse>();
            Assert.IsNotNull(locationEmployee);
            Assert.AreEqual(location.Id, locationEmployee.Id);
            Assert.IsFalse(locationEmployee.Employees.Any(x => x.Id == employee.Id));
        }

        private async Task<EmployeeResponse> CreateEmployee(HttpClient httpClient)
        {
            var employee = new EmployeeCreateRequest
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "Email@test.com",
                PhoneNumber = "1234567890",
            };
            var createdEmployee = await CreateEmployee(employee, httpClient);
            return createdEmployee;
        }

        private async Task<EmployeeResponse> CreateEmployee
            (EmployeeCreateRequest employee, HttpClient httpClient)
        {
            var response = await httpClient.PostAsJsonAsync($"{_employeeUri}/create", employee);
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to create employee");

            var createdEmployee = await response.Content.ReadFromJsonAsync<EmployeeResponse>();
            Assert.IsNotNull(createdEmployee);
            return createdEmployee;
        }

        private async Task<BusinessLocationResponse> CreateLocation(HttpClient httpClient)
        {
            var location = new BusinessLocationCreateRequest
            {
                Name = "TestFirstName",
                Address = "TestLastName",
                PhoneNumber = "1234567890",
            };
            var createdLocation = await CreateLocation(location, httpClient);
            return createdLocation;
        }

        private async Task<BusinessLocationResponse> CreateLocation
            (BusinessLocationCreateRequest location, HttpClient httpClient)
        {
            var response = await httpClient.PostAsJsonAsync($"{_baseUri}/create", location);
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to create location");

            var createdLocation = await response.Content
                .ReadFromJsonAsync<BusinessLocationResponse>();
            Assert.IsNotNull(createdLocation);
            return createdLocation;
        }

        private async Task<BusinessLocationEntity> GetLocationFromDbAsync(int locationId)
        {
            var location = await _dbExecuter.Query<BusinessLocationEntity>($"SELECT * FROM [dbo].[BusinessLocations] s WHERE s.Id = {locationId}");
            return location.FirstOrDefault();
        }
    }
}

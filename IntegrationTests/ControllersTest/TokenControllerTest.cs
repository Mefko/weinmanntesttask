﻿using IntegrationTests.Db;
using IntegrationTests.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http.Json;
using System.Net;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace IntegrationTests.ControllersTest
{
    [TestClass]
    public class TokenControllerTest
    {
        private readonly string _baseUri = "Token";
        private readonly ServerApiFactory _factory = new();
        private HttpClient _httpClient = null;
        private IDbExecuter _dbExecuter = null;

        [TestInitialize]
        public void Init()
        {
            _httpClient = _factory.CreateClient();
            _dbExecuter = _factory.GetDbExecuter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbExecuter.Execute("delete from [dbo].[Customers]");
        }

        [TestMethod]
        public async Task UserLogin_BadCreds_Unauthorized()
        {
            var logModel = new CustomerLoginRequest()
            {
                Number = 21312,
                Password = "dsdsd"
            };

            var logInResponce = await _httpClient.PostAsJsonAsync($"{_baseUri}/login", logModel);
            Assert.AreEqual(HttpStatusCode.Unauthorized, logInResponce.StatusCode);
        }

        [TestMethod]
        public async Task UserLogin_Auth_ValidToken()
        {
            var customerToCreate = new CustomerCreateRequest()
            {
                Name = $"TestName{Guid.NewGuid()}",
                Password = "TestPwd"
            };

            var customer = await CreateCustomer(customerToCreate);
            Assert.IsNotNull(customer);

            var logModel = new CustomerLoginRequest()
            {
                Number = customer.Number,
                Password = customerToCreate.Password
            };

            var logInResponce = await _httpClient.PostAsJsonAsync($"{_baseUri}/login", logModel);
            Assert.IsTrue(logInResponce.IsSuccessStatusCode);
            var token = await logInResponce.Content.ReadFromJsonAsync<TokenResponse>();
            Assert.IsNotNull(token);
            Assert.IsNotNull(token.Token);
            Assert.IsNotNull(token.RefreshToken);
        }

        private async Task<CustomerResponse> CreateCustomer(CustomerCreateRequest customer)
        {
            var createCustomerRespone = await _httpClient.PostAsJsonAsync($"Customers/create", customer);

            Assert.IsTrue(createCustomerRespone.IsSuccessStatusCode, "Failed to create customer");
            var createdCustomer = await createCustomerRespone.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(createdCustomer, "Response can't be null");
            return createdCustomer;
        }
    }
}

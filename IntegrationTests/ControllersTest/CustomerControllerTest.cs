﻿using Domain.Models;
using Domain.Models.Entities;
using IntegrationTests.Db;
using IntegrationTests.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Http.Json;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace IntegrationTests.ControllersTest
{
    [TestClass]
    public class CustomerControllerTest
    {
        private readonly string _baseUri = "Customers";
        private readonly ServerApiFactory _factory = new();
        private HttpClient _httpClient = null;
        private IDbExecuter _dbExecuter = null;

        [TestInitialize]
        public void Init()
        {
            _httpClient = _factory.CreateClient();
            _dbExecuter = _factory.GetDbExecuter();
        }

        [TestCleanup]
        public void CleanUp()
        {
            _dbExecuter.Execute("delete from [dbo].[Customers]");
        }

        [TestMethod]
        public async Task Create_CustomerCreated_Get()
        {
            var customerToCreate = new CustomerCreateRequest()
            {
                Name = "TestName",
                Password = "TestPassword"
            };
            var createdCustomer = await CreateCustomer(customerToCreate);
            var dbCustomer = await GetCustomerByNumberFromDbAsync(createdCustomer.Number);
            Assert.IsNotNull(dbCustomer, "Customer not exist in DB");
            Assert.AreEqual(dbCustomer.Name, createdCustomer.Name, "Name from response are different from DB");
            Assert.AreEqual(dbCustomer.Number, createdCustomer.Number, "Number from response are different from DB");
        }

        [TestMethod]
        public async Task GetAll_CreateCustomers_GetForbidden()
        {
            var customers = new List<CustomerCreateRequest>()
            {
                new CustomerCreateRequest() {Name = "Test1", Password = "TestPassword"},
                new CustomerCreateRequest() {Name = "Test2", Password = "TestPassword"}
            };
            foreach (var customer in customers)
            {
                await CreateCustomer(customer);
            }
            var adminUserAgent = await _factory.CreateAuthHttpClient();
            var response = await adminUserAgent.HttpClient.GetAsync($"{_baseUri}/all");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [TestMethod]
        public async Task GetAll_CreateCustomers_GetUnauthorized()
        {
            var customers = new List<CustomerCreateRequest>()
            {
                new CustomerCreateRequest() {Name = "Test1", Password = "TestPassword"},
                new CustomerCreateRequest() {Name = "Test2", Password = "TestPassword"}
            };
            foreach (var customer in customers)
            {
                await CreateCustomer(customer);
            }
            
            var response = await _httpClient.GetAsync($"{_baseUri}/all");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task GetAll_CreateCustomers_GetOk()
        {
            var customers = new List<CustomerCreateRequest>()
            {
                new CustomerCreateRequest() {Name = "Test1", Password = "TestPassword"},
                new CustomerCreateRequest() {Name = "Test2", Password = "TestPassword"}
            };
            foreach (var customer in customers)
            {
                await CreateCustomer(customer);
            }
            var adminUserAgent = await _factory.CreateAdminAuthHttpClient();
            var userCount = customers.Count + 1;
            var response = await adminUserAgent.HttpClient.GetAsync($"{_baseUri}/all");
            Assert.IsTrue(response.IsSuccessStatusCode, " get all customers");
            var customersResponse = await response.Content.ReadFromJsonAsync<IEnumerable<CustomerResponse>>();
            Assert.IsNotNull(customersResponse);
            Assert.AreEqual(userCount, customersResponse.Count());

            var customersFromDb = await GetCustomersFromDbAsync();
            Assert.IsNotNull(customersFromDb);
            Assert.AreEqual(userCount, customersFromDb.Count());
        }

        [TestMethod]
        public async Task Get_CreateCustomers_GetForbidden()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var customerAgent = await _factory.CreateAuthHttpClient(customer);

            var response = await customerAgent.HttpClient.GetAsync($"{_baseUri}/{customerAgent.CustomerNumber}");
            
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [TestMethod]
        public async Task Get_CreateCustomers_GetUnauthorized()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);

            var response = await _httpClient.GetAsync($"{_baseUri}/{createdCustomer.Number}");

            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Get_CreateCustomers_GetSingle()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var customerAgent = await _factory.CreateAuthHttpClient(customer, true);

            var response = await customerAgent.HttpClient.GetAsync($"{_baseUri}/{customerAgent.CustomerNumber}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to get customer");
            var customersResponse = await response.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(customersResponse);
            Assert.AreEqual(customerAgent.CustomerNumber, customersResponse.Number);
            Assert.AreEqual(customer.Name, customersResponse.Name);
        }

        [TestMethod]
        public async Task GetMy_CreateCustomers_GetUnauthorized()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            await CreateCustomer(customer);

            var response = await _httpClient.GetAsync($"{_baseUri}");

            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task GetMy_CreateCustomers_GetSingle()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var customerAgent = await _factory.CreateAuthHttpClient(customer);

            var response = await customerAgent.HttpClient.GetAsync($"{_baseUri}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to get customer");
            var customersResponse = await response.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(customersResponse);
            Assert.AreEqual(customerAgent.CustomerNumber, customersResponse.Number);
            Assert.AreEqual(customer.Name, customersResponse.Name);
        }

        [TestMethod]
        public async Task Upate_CreateCustomersAndUpdate_GetUnauthorized()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);
            var updateRequest = new CustomerUpdateRequest()
            {
                Name = "UpdatedName",
            };
            
            var response = await _httpClient.PutAsJsonAsync($"{_baseUri}/update/{createdCustomer.Number}", updateRequest);
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Upate_CreateCustomersAndUpdate_Forbidden()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);
            var updateRequest = new CustomerUpdateRequest()
            {
                Name = "UpdatedName",
            };
            var customerAgent = await _factory.CreateAuthHttpClient();
            var response = await customerAgent.HttpClient.PutAsJsonAsync($"{_baseUri}/update/{createdCustomer.Number}", updateRequest);
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [TestMethod]
        public async Task Upate_CreateCustomersAndUpdate_SameInDb()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);
            var updateRequest = new CustomerUpdateRequest()
            {
                Name = "UpdatedName",
            };
            var customerAgent = await _factory.CreateAdminAuthHttpClient();
            var response = await customerAgent.HttpClient.PutAsJsonAsync($"{_baseUri}/update/{createdCustomer.Number}", updateRequest);
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to update customer");
            var customersResponse = await response.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(customersResponse);
            Assert.AreEqual(createdCustomer.Number, customersResponse.Number);
            Assert.AreEqual(updateRequest.Name, customersResponse.Name);
            Assert.AreNotEqual(createdCustomer.Name, customersResponse.Name);
            var customerFromDb = await GetCustomerByNumberFromDbAsync(createdCustomer.Number);
            Assert.AreEqual(customerFromDb.Name, customersResponse.Name);
        }

        [TestMethod]
        public async Task UpateMy_CreateCustomersAndUpdate_GetUnauthorized()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            await CreateCustomer(customer);
            var updateRequest = new CustomerUpdateRequest()
            {
                Name = "UpdatedName",
            };

            var response = await _httpClient.PutAsJsonAsync($"{_baseUri}/update", updateRequest);
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task UpateMy_CreateCustomersAndUpdate_SameInDb()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var updateRequest = new CustomerUpdateRequest()
            {
                Name = "UpdatedName",
            };
            var customerAgent = await _factory.CreateAuthHttpClient(customer);
            var response = await customerAgent.HttpClient.PutAsJsonAsync($"{_baseUri}/update", updateRequest);
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to update customer");
            var customersResponse = await response.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(customersResponse);
            Assert.AreEqual(customerAgent.CustomerNumber, customersResponse.Number);
            Assert.AreEqual(updateRequest.Name, customersResponse.Name);
            Assert.AreNotEqual(customer.Name, customersResponse.Name);
            var customerFromDb = await GetCustomerByNumberFromDbAsync(customerAgent.CustomerNumber);
            Assert.AreEqual(customerFromDb.Name, customersResponse.Name);
        }

        [TestMethod]
        public async Task Delete_CreateCustomersAndDelete_GetForbidden()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);
            var customerAgent = await _factory.CreateAuthHttpClient();
            var response = await customerAgent.HttpClient.DeleteAsync($"{_baseUri}/{createdCustomer.Number}");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [TestMethod]
        public async Task Delete_CreateCustomersAndDelete_GetUnauthorized()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);

            var response = await _httpClient.DeleteAsync($"{_baseUri}/{createdCustomer.Number}");
            Assert.IsFalse(response.IsSuccessStatusCode, "Role validation failed");
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [TestMethod]
        public async Task Delete_CreateCustomersAndDelete_Ok()
        {
            var customer = new CustomerCreateRequest() { Name = "Test1", Password = "TestPassword" };

            var createdCustomer = await CreateCustomer(customer);
            var customerAgent = await _factory.CreateAdminAuthHttpClient();
            var response = await customerAgent.HttpClient.DeleteAsync($"{_baseUri}/{createdCustomer.Number}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Failed to delete customer");
            var customerDleteResponse = await response.Content.ReadFromJsonAsync<bool>();
            Assert.IsTrue(customerDleteResponse, "Failed to delete customer");
            
            var customerFromDb = await GetCustomerByNumberFromDbAsync(createdCustomer.Number);
            Assert.IsNull(customerFromDb, "Customer wasn't deleted from DB");
        }

        private async Task<CustomerResponse> CreateCustomer(CustomerCreateRequest customer)
        {
            var createCustomerRespone = await _httpClient.PostAsJsonAsync($"{_baseUri}/create", customer);

            Assert.IsTrue(createCustomerRespone.IsSuccessStatusCode, "Failed to create customer");
            var createdCustomer = await createCustomerRespone.Content.ReadFromJsonAsync<CustomerResponse>();
            Assert.IsNotNull(createdCustomer, "Response can't be null");
            return createdCustomer;
        }

        private async Task<CustomerEntity> GetCustomerByNumberFromDbAsync(int number)
        {
            var customer = await _dbExecuter.Query<CustomerEntity>($"SELECT * FROM [dbo].[Customers] s WHERE s.Number = {number}");
            return customer.FirstOrDefault();
        }

        private async Task<IEnumerable<CustomerEntity>> GetCustomersFromDbAsync()
        {
            var customer = await _dbExecuter.Query<CustomerEntity>($"SELECT * FROM [dbo].[Customers]");
            return customer;
        }
    }
}

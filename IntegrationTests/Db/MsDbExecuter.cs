﻿using Microsoft.Data.SqlClient;
using Dapper;

namespace IntegrationTests.Db
{
    internal class MsDbExecuter : IDbExecuter
    {
        private readonly string _connectionString;

        public MsDbExecuter(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Execute(string query)
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            await sqlConnection.OpenAsync();
            using var sqlCommand = new SqlCommand(query, sqlConnection);
            return sqlCommand.ExecuteNonQuery();
        }

        public async Task<IEnumerable<T>> Query<T>(string query) where T : class
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            await sqlConnection.OpenAsync();
            var result = await sqlConnection.QueryAsync<T>(query);
            return result;
        }
    }
}

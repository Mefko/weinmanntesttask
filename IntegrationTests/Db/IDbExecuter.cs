﻿namespace IntegrationTests.Db
{
    internal interface IDbExecuter
    {
        Task<IEnumerable<T>> Query<T>(string query) where T : class;

        Task<int> Execute(string qery);
    }
}

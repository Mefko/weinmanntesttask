﻿namespace WebApiTestTask.Models.Responses
{
    public class BusinessLocationResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}

﻿namespace WebApiTestTask.Models.Responses
{
    public record TokenResponse
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}

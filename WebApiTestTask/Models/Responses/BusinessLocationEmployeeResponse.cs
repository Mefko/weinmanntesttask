﻿namespace WebApiTestTask.Models.Responses
{
    public class BusinessLocationEmployeeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public IList<EmployeeResponse> Employees { get; set; }
    }
}

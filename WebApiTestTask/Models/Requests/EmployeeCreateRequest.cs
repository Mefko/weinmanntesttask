﻿using System.ComponentModel.DataAnnotations;

namespace WebApiTestTask.Models.Requests
{
    public record EmployeeCreateRequest
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }
}

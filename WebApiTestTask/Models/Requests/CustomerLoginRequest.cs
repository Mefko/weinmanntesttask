﻿using System.ComponentModel.DataAnnotations;

namespace WebApiTestTask.Models.Requests
{
    public record CustomerLoginRequest
    {
        [Required]
        public int Number { get; set; }
        [Required]
        public string Password {  get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace WebApiTestTask.Models.Requests
{
    public record AddLocationEmployeeRequest
    {
        [Required]
        public int BusinessLocationId { get; set; }
        [Required]
        public int EmployeeId { get; set; }
    }
}

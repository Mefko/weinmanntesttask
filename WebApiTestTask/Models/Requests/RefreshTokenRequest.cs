﻿using System.ComponentModel.DataAnnotations;

namespace WebApiTestTask.Models.Requests
{
    public record RefreshTokenRequest
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}

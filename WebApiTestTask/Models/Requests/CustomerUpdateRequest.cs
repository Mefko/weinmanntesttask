﻿using System.ComponentModel.DataAnnotations;

namespace WebApiTestTask.Models.Requests
{
    public record CustomerUpdateRequest
    {
        [Required]
        public string Name { get; set; }
    }
}

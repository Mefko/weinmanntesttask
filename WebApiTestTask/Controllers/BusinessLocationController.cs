﻿using AutoMapper;
using Domain.Contracts.Services;
using Domain.Models;
using Domain.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiTestTask.Extensions;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class BusinessLocationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IBusinessLocationService _businessLocationService;

        public BusinessLocationController(IMapper mapper, 
            IBusinessLocationService businessLocationService)
        {
            _mapper = mapper;
            _businessLocationService = businessLocationService;
        }

        [HttpPost("create")]
        [ProducesResponseType(typeof(BusinessLocationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> CreateAsync([FromBody] BusinessLocationCreateRequest createRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var location = _mapper.Map<BusinessLocation>(createRequest);
            var customerNumber = User.GetCustomerNumber();
            location.CustomerNumber = customerNumber;
            location = await _businessLocationService.CreateAsync(location);
            var response = _mapper.Map<BusinessLocationResponse>(location);
            return Ok(response);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BusinessLocationEmployeeResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> GetAsync()
        {
            var customerNumber = User.GetCustomerNumber();
            var locations = await _businessLocationService
                .GetLocationWithEmployeeByCustomerAsync(customerNumber);
            var response = _mapper.Map<IEnumerable<BusinessLocationEmployeeResponse>>(locations);
            return Ok(response);
        }

        [HttpPut("update/{id}")]
        [ProducesResponseType(typeof(BusinessLocationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] BusinessLocationUpdateRequest updateRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customerNumber = User.GetCustomerNumber();
            var location = _mapper.Map<BusinessLocation>(updateRequest);
            location.CustomerNumber = customerNumber;
            location.Id = id;
            var response = await _businessLocationService.UpdateAsync(location);
            if (response is null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var customerNumber = User.GetCustomerNumber();
            var response = await _businessLocationService.DeleteAsyc(id, customerNumber);
            return Ok(response);
        }

        [HttpPost("add/employee")]
        [ProducesResponseType(typeof(BusinessLocationEmployeeResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> AddEmployeeAsync([FromBody]AddLocationEmployeeRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var customerNumber = User.GetCustomerNumber();
                var location = await _businessLocationService
                    .AddEmployeeToLocation(request.BusinessLocationId, request.EmployeeId, customerNumber);
                var response = _mapper.Map<BusinessLocationEmployeeResponse>(location);
                return Ok(response);
            } catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("{locationId}/remove/employee/{employeeId}")]
        [ProducesResponseType(typeof(BusinessLocationEmployeeResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> RemoveEmployeeAsync(int locationId, int employeeId)
        {
            try
            {
                var customerNumber = User.GetCustomerNumber();
                var location = await _businessLocationService
                    .RemoveEmployeeToLocation(locationId, employeeId, customerNumber);
                var response = _mapper.Map<BusinessLocationEmployeeResponse>(location);
                return Ok(response);
            } catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

using AutoMapper;
using Domain.Contracts;
using Domain.Contracts.Services;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiTestTask.Extensions;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ILogger<CustomersController> logger,
            ICustomerService customerService,
            IMapper mapper)
        {
            _logger = logger;
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpGet("all")]
        [Authorize(Roles = CustomerRoles.Admin)]
        [ProducesResponseType(typeof(IEnumerable<CustomerResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> GetAllAsync()
        {
            var customers = await _customerService.GetAllAsync();
            var response = _mapper.Map<IEnumerable<CustomerResponse>>(customers);
            return Ok(response);
        }

        [HttpGet("{customerNumber}")]
        [Authorize(Roles = CustomerRoles.Admin)]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> GetAsync(int customerNumber)
        {
            var response = await _customerService.GetByNumberAsync(customerNumber);
            if(response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [HttpGet()]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> GetMyAsync()
        {
            var customerNumber = User.GetCustomerNumber();
            var response = await _customerService.GetByNumberAsync(customerNumber);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("create")]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody]CustomerCreateRequest customerRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customer = _mapper.Map<Customer>(customerRequest);
            customer = await _customerService.CreateAsync(customer, customerRequest.Password);
            var response = _mapper.Map<CustomerResponse>(customer);
            return Ok(response);
        }

        [HttpPut("update/{number}")]
        [Authorize(Roles = CustomerRoles.Admin)]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> UpdateAsync(int number, [FromBody] CustomerUpdateRequest updateCustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customer = _mapper.Map<Customer>(updateCustomer);
            customer.Number = number;
            var response = await _customerService.UpdateAsync(customer);
            if(response is null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [HttpPut("update")]
        [ProducesResponseType(typeof(CustomerResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> UpdateMyAsync([FromBody] CustomerUpdateRequest updateCustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customer = _mapper.Map<Customer>(updateCustomer);
            customer.Number = User.GetCustomerNumber();
            var response = await _customerService.UpdateAsync(customer);
            if (response is null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [HttpDelete("{customerNumber}")]
        [Authorize(Roles = CustomerRoles.Admin)]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> DeleteAsync(int customerNumber)
        {
            var response = await _customerService.DeleteByNumberAsync(customerNumber);
            return Ok(response);
        }
    }
}
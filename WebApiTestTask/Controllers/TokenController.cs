﻿using AutoMapper;
using Domain.Contracts.Services;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TokenController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        public TokenController(IMapper mapper, ITokenService tokenService)
        {
            _mapper = mapper;
            _tokenService = tokenService;
        }

        [HttpPost("login")]
        [ProducesResponseType(typeof(TokenResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UserLogin([FromBody] CustomerLoginRequest loginRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var logInModel = _mapper.Map<CustomerAuth>(loginRequest);
            var token = await _tokenService.LoginAsync(logInModel);
            if (token == null)
            {
                return Unauthorized();
            }
            var responceToken = _mapper.Map<TokenResponse>(token);
            return Ok(responceToken);
        }

        [HttpPost("refresh")]
        [ProducesResponseType(typeof(TokenResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest tokenRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var refreshToken = _mapper.Map<RefreshToken>(tokenRequest);
            var refreshedToken = await _tokenService.RefreshTokenAsync(refreshToken);
            if (refreshedToken == null)
            {
                return BadRequest("Invalid token");
            }
            var refreshedTokenResponce = _mapper.Map<TokenResponse>(refreshedToken);
            return Ok(refreshedTokenResponce);
        }
    }
}

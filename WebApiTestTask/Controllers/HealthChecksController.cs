﻿using Domain.Contracts.Db;
using Microsoft.AspNetCore.Mvc;

namespace WebApiTestTask.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthChecksController : Controller
    {
        private readonly ITestTaskDbContext _dbContext;
        public HealthChecksController(ITestTaskDbContext db)
        {
            _dbContext = db;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        public IActionResult HealthChecks()
        {
            var dbConnectionIsAlive = _dbContext.IsAlive();
            var checks = new List<string>
            {
                "service: alive",
                $"Main Data base connection: {(dbConnectionIsAlive ? "alive" :"dead" )}",
            };
            return Ok(checks);
        }
    }
}

﻿using Domain.Contracts.Db;
using Microsoft.OpenApi.Models;
using WebApiTestTask.Extensions;
using WebApiTestTask.Swagger;

namespace WebApiTestTask
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen((c) =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "TestTask API", Version = "v1" });
                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Scheme = "bearer"
                });
                c.OperationFilter<AuthSwaggerOperationFilter>();

            });

            services.AddModelMapping();

            services.AddConfigurations(Configuration);
            services.AddInfrastructureServices();
            
            services.ConfigureDbContext();
            services.AddRepositories();

            services.AddDomainServices();

            services.SetupAuthServices(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment environment)
        {
            var swaggerEnabled = Configuration.GetValue<bool>("Swagger:IsEnabled");
            if (swaggerEnabled)
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            if (environment.IsDevelopment())
            {
                using IServiceScope serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var dbContext = serviceScope.ServiceProvider.GetService<ITestTaskDbContext>();
                dbContext.Migrate();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}

﻿using System.Security.Claims;

namespace WebApiTestTask.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static int GetCustomerNumber(this ClaimsPrincipal claims)
        {
            var strCustomerNumber = claims.FindFirstValue(ClaimTypes.NameIdentifier);
            if (string.IsNullOrEmpty(strCustomerNumber))
            {
                throw new ArgumentException($"Claims shoul contain {ClaimTypes.NameIdentifier}");
            }
            if(int.TryParse(strCustomerNumber, out var customerNumber))
            {
                return customerNumber;
            }
            throw new ArgumentException($"Claim {ClaimTypes.NameIdentifier} contain not int");
        }
    }
}

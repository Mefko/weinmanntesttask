﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Services;
using Infrastructure.Db;
using Infrastructure.Db.Models;
using Infrastructure.Db.Repositories;
using Infrastructure.IdentityProvider;
using Infrastructure.IdentityProvider.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using System.Text;
using WebApiTestTask.Mapping;

namespace WebApiTestTask.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection ConfigureDbContext(this IServiceCollection services)
        {
            services.AddDbContext<ITestTaskDbContext, TestTaskDbContext>((serviceProvider, options) =>
            {
                var dbConnectionFactory = serviceProvider.GetRequiredService<IDatabaseConnectionFactory>();
                var dbSettings = dbConnectionFactory.GetSettings();
                options.UseSqlServer(dbConnectionFactory.GetContentDbConnection(), 
                    sqlOptions => {
                        sqlOptions.MigrationsAssembly("Infrastructure");
                        sqlOptions.EnableRetryOnFailure(dbSettings.RetryCount,
                            TimeSpan.FromSeconds(dbSettings.RetryCount), null);
                    });

            });
            return services;
        }

        public static IServiceCollection AddConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DbSettings>(configuration.GetSection("DbSettings"));
            services.Configure<JWTSettings>(configuration.GetSection("JWT"));
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICustomerRolesRepository, CustomerRolesRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IBusinessLocationRepository, BusinessLocationRepository>();
            services.AddScoped<IEmployeeBusinessLocationRepository, EmployeeBusinessLocationRepository>();
            return services;
        }
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
        {
            services.AddScoped<IHashService, HashService>();
            services.AddScoped<ITokenGenerator, TokenGenerator>();
            services.AddSingleton<IDatabaseConnectionFactory, DatabaseConnectionFactory>();
            return services;
        }

        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IBusinessLocationService, BusinessLocationService>();
            return services;
        }

        public static IServiceCollection AddModelMapping(this IServiceCollection services)
        {
            services.AddAutoMapper(
                Assembly.GetAssembly(typeof(CustomerProfile)),
                Assembly.GetAssembly(typeof(TokenProfile)),
                Assembly.GetAssembly(typeof(EmployeeProfile)),
                Assembly.GetAssembly(typeof(BusinessLocationProfile))
            );
            return services;
        }

        public static IServiceCollection SetupAuthServices(this IServiceCollection services, IConfiguration configuration)
        {
            var secret = configuration.GetValue<string>("JWT:Secret");
            if (string.IsNullOrEmpty(secret))
            {
                throw new ArgumentNullException("Secret neet to be provided");
            }
            var tokenValidationParams = new TokenValidationParameters()
            {

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = true;
                options.TokenValidationParameters = tokenValidationParams;
            });
            services.AddSingleton(tokenValidationParams);
            return services;
        }

    }
}

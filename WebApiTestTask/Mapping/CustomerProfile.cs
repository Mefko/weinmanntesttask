﻿using AutoMapper;
using Domain.Models.Entities;
using Domain.Models;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Mapping
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, CustomerEntity>();
            CreateMap<CustomerEntity, Customer>();
            CreateMap<CustomerCreateRequest, Customer>();
            CreateMap<Customer, CustomerResponse>();
            CreateMap<CustomerUpdateRequest, Customer>();
        }
    }
}

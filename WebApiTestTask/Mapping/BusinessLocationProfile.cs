﻿using AutoMapper;
using Domain.Models;
using Domain.Models.Entities;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Mapping
{
    public class BusinessLocationProfile : Profile
    {
        public BusinessLocationProfile()
        {
            CreateMap<BusinessLocation, BusinessLocationEntity>();
            CreateMap<BusinessLocationEntity, BusinessLocation>();
            CreateMap<BusinessLocationEntity, BusinessLocationEmployee>();
            CreateMap<BusinessLocationEmployee, BusinessLocationEmployeeResponse>();
            CreateMap<BusinessLocation, BusinessLocationResponse>();
            CreateMap<BusinessLocationCreateRequest, BusinessLocation>();
            CreateMap<BusinessLocationUpdateRequest, BusinessLocation>();
        }
    }
}

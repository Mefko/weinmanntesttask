﻿using AutoMapper;
using Domain.Models;
using Domain.Models.Entities;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Mapping
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<Employee, EmployeeEntity>();
            CreateMap<EmployeeEntity, Employee>();
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<EmployeeCreateRequest, Employee>();
            CreateMap<EmployeeUpdateRequest,  Employee>();
        }
    }
}

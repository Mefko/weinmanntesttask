﻿using AutoMapper;
using Domain.Models;
using WebApiTestTask.Models.Requests;
using WebApiTestTask.Models.Responses;

namespace WebApiTestTask.Mapping
{
    public class TokenProfile : Profile
    {
        public TokenProfile()
        {
            CreateMap<CustomerLoginRequest, CustomerAuth>();
            CreateMap<Token, TokenResponse>()
                .ForMember(m=> m.Token, opt=>opt.MapFrom(f=>f.TokenValue));
            CreateMap<RefreshTokenRequest, RefreshToken>();
        }
    }
}

﻿namespace Infrastructure.IdentityProvider.Models
{
    public record JWTSettings
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int TokenTtl { get; set; }
        public int RefreshTokenTttl { get; set; }
    }
}

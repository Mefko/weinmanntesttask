﻿using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Models;
using Infrastructure.IdentityProvider.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Infrastructure.IdentityProvider
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly IOptions<JWTSettings> _jwtSetttings;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly ICustomerRolesRepository _customerRolesRepository;

        public TokenGenerator(IOptions<JWTSettings> jwtSetttings,
            ICustomerRolesRepository customerRolesRepository,
            TokenValidationParameters tokenValidationParameters)
        {
            _jwtSetttings = jwtSetttings;
            _tokenValidationParameters = tokenValidationParameters;
            _customerRolesRepository = customerRolesRepository;
        }

        public async Task<Token> GenerateAsync(Customer customer, RefreshToken refreshToken)
        {
            var authClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, customer.Number.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var roles = await _customerRolesRepository.GetRolesByCustomerAsync(customer.Number);
            foreach (var role in roles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role.Name));
            }
            var authSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtSetttings.Value.Secret));
            var token = new JwtSecurityToken(
                issuer: _jwtSetttings.Value.Issuer,
                audience: _jwtSetttings.Value.Audience,
                expires: DateTime.UtcNow.AddMinutes(_jwtSetttings.Value.TokenTtl),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            var tokenResult = new Token()
            {
                TokenValue = jwtToken,
                RefreshToken = string.Empty,
                ExpiresAt = token.ValidTo
            };
            await Task.Delay(0);
            return tokenResult;
        }

        public Task<Token> RegenerateAsync(RefreshToken refreshToken)
        {
            throw new NotImplementedException();
        }
    }
}

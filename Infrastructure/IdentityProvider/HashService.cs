﻿using Domain.Contracts.Services;
using System.Security.Cryptography;
using System.Text;

namespace Infrastructure.IdentityProvider
{
    public class HashService : IHashService
    {
        public string GetHash(string value)
        {
            string hash = String.Empty;
            using var sha256 = SHA256.Create();
            byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(value));
            foreach (byte b in hashValue)
            {
                hash += $"{b:X2}";
            }
            return hash;
        }
    }
}

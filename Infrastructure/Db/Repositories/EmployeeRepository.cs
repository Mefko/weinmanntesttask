﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db.Repositories
{
    public class EmployeeRepository : RepositoryBase<EmployeeEntity>, IEmployeeRepository
    {
        public EmployeeRepository(ITestTaskDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<EmployeeEntity>> GetByCustomerAsync(int customerNumber)
        {
            var query = DbSet.Where(e => e.CustomerNumber == customerNumber);
            var result = await query.ToListAsync();
            return result;
        }

        public async Task<EmployeeEntity> GetByIdAndCustomerAsync(int id, int customerNumber)
        {
            var query = DbSet.Where(e => e.Id == id && e.CustomerNumber == customerNumber);
            var result = await query.FirstOrDefaultAsync();
            return result;
        }
    }
}

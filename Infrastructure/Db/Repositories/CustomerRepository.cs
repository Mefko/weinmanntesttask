﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Models.Entities;

namespace Infrastructure.Db.Repositories
{
    public class CustomerRepository : RepositoryBase<CustomerEntity>, ICustomerRepository
    {
        public CustomerRepository(ITestTaskDbContext dbContext) : base(dbContext)
        {
        }
    }
}

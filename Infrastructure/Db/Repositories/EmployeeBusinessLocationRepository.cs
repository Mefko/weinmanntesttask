﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db.Repositories
{
    public class EmployeeBusinessLocationRepository : 
        RepositoryBase<EmployeeBusinessLocationEntity>, IEmployeeBusinessLocationRepository
    {
        public EmployeeBusinessLocationRepository(ITestTaskDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<EmployeeBusinessLocationEntity> GetByCustomerIdAsync(int locationId, int employeeId)
        {
            var query = DbSet
                .Where(e => e.EmployeeId == employeeId
                    && e.BusinessLocationId == locationId);
            var result = await query.FirstOrDefaultAsync();
            return result;
        }
    }
}

﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db.Repositories
{
    public class CustomerRolesRepository : RepositoryBase<CustomerRoleEntity>, ICustomerRolesRepository
    {
        public CustomerRolesRepository(ITestTaskDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<RoleEntity>> GetRolesByCustomerAsync(int customerNumber)
        {
            var query = DbSet.Include(e => e.Role)
                .Where(cr => cr.CustomerId == customerNumber)
                .Select(cr=> cr.Role);
            var dbResult = await query.ToListAsync();
            return dbResult;
        }
    }
}

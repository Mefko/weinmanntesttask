﻿using Domain.Contracts.Db;
using Domain.Contracts.Db.Repositories;
using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db.Repositories
{
    public class BusinessLocationRepository : RepositoryBase<BusinessLocationEntity>, IBusinessLocationRepository
    {
        public BusinessLocationRepository(ITestTaskDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<BusinessLocationEntity>> GetByCustomerAsync(
            int customerNumber, 
            bool includeEmployees = false)
        {
            var query = DbSet.AsQueryable();
            if (includeEmployees)
            {
                query.Include(q => q.Employees);
            }
            query = query.Where(e => e.CustomerNumber == customerNumber);
            var result = await query.ToListAsync();
            return result;
        }

        public async Task<BusinessLocationEntity> GetByIdAndCustomerAsync(
            int id, 
            int customerNumber, 
            bool includeEmployees = false)
        {
            var query = DbSet.AsQueryable();
            if (includeEmployees)
            {
                query.Include(q => q.Employees);
            }
            query = DbSet.Where(e => e.Id == id && e.CustomerNumber == customerNumber);
            var result = await query.FirstOrDefaultAsync();
            return result;
        }
    }
}

﻿using Infrastructure.Db.Models;
using System.Data.Common;

namespace Infrastructure.Db
{
    public interface IDatabaseConnectionFactory
    {
        DbConnection GetContentDbConnection();

        string GetConnectionString();

        DbSettings GetSettings();
    }
}

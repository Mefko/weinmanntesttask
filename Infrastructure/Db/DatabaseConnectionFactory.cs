﻿using Infrastructure.Db.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using System.Data.Common;

namespace Infrastructure.Db
{
    public class DatabaseConnectionFactory : IDatabaseConnectionFactory
    {
        private readonly DbSettings _dbSettings;

        public DatabaseConnectionFactory(IOptions<DbSettings> dbSettings)
        {
            _dbSettings = dbSettings.Value;
        }

        public string GetConnectionString()
        {
            return _dbSettings.ConnectionString;
        }

        public DbConnection GetContentDbConnection()
        {   
            return new SqlConnection(_dbSettings.ConnectionString);
        }

        public DbSettings GetSettings()
        {
            return _dbSettings;
        }
    }
}

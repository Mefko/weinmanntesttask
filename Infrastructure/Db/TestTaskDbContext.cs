﻿using Domain.Contracts.Db;
using Domain.Models.Entities;
using Infrastructure.Db.EntityConfiguration;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db
{
    public class TestTaskDbContext : DbContext, ITestTaskDbContext
    {
        public DbSet<CustomerEntity> Customers { get; set; }
        public DbSet<CustomerEntity> Roles { get; set; }
        public DbSet<CustomerRoleEntity> CustomerRoles { get; set; }
        public DbSet<BusinessLocationEntity> BusinessLocations { get; set; }
        public DbSet<EmployeeEntity> Employees { get; set; }

        public TestTaskDbContext(DbContextOptions<TestTaskDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CustomersConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerRoleConfiguration());
            modelBuilder.ApplyConfiguration(new BusinessLocationConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeBusinessLocationConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public bool IsAlive()
        {
            try
            {
                return Database.CanConnect();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Migrate()
        {
            Database.Migrate();
        }
    }
}

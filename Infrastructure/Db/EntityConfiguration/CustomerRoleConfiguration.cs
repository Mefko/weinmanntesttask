﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public class CustomerRoleConfiguration : BaseConfiguration<CustomerRoleEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<CustomerRoleEntity> builder)
        {   
            builder.ToTable("CustomerRoles");            
            builder.HasKey(e => new { e.CustomerId, e.RoleId });
        }
    }
}

﻿using Domain.Models.Entities.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public abstract class BaseConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : EntityBase
    {
        public void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property(p => p.Version)
                //.HasDefaultValueSql("NEWID()")
                //.ValueGeneratedOnAddOrUpdate()
                .IsRequired()
                .IsRowVersion()
                .ValueGeneratedOnAddOrUpdate();
                //.IsConcurrencyToken();
            ConfigureEntity(builder);
        }

        public abstract void ConfigureEntity(EntityTypeBuilder<TEntity> builder);

    }
}

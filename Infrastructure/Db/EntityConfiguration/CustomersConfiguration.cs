﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    internal class CustomersConfiguration : BaseConfiguration<CustomerEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<CustomerEntity> builder)
        {
            builder.ToTable("Customers");
            builder.HasKey(e => new { e.Number });
            builder.Property(p=> p.Number)
                .IsRequired()
                .UseIdentityColumn();
            builder.Property(p=> p.Name).HasMaxLength(255);
            builder.Property(p => p.Password).HasMaxLength(255);
            builder.HasMany(e => e.Roles)
                .WithMany(e => e.Customers)
                .UsingEntity<CustomerRoleEntity>();
            builder.Property(p => p.Version)
                .ValueGeneratedOnAddOrUpdate()
                .IsRequired()
                .IsConcurrencyToken();
        }
    }
}

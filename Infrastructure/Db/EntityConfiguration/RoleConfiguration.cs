﻿using Domain.Contracts;
using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public class RoleConfiguration : BaseConfiguration<RoleEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<RoleEntity> builder)
        {
            builder.ToTable("Roles");
            builder.HasKey(e => new { e.Id });
            builder.Property(p => p.Id)
                .IsRequired()
                .UseIdentityColumn();
            builder.Property(p => p.Name).HasMaxLength(255);
            builder.HasMany(e => e.Customers)
                .WithMany(e => e.Roles)
                .UsingEntity<CustomerRoleEntity>();
            Seed(builder);
        }

        private static void Seed(EntityTypeBuilder<RoleEntity> builder)
        {
            builder.HasData(
                new RoleEntity { Id = 1, Name = CustomerRoles.Admin }
            );
        }
    }
}

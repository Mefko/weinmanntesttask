﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public class EmployeeConfiguration : BaseConfiguration<EmployeeEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<EmployeeEntity> builder)
        {
            builder.ToTable("Employee");
            builder.HasKey(e => new { e.Id });
            builder.Property(p => p.Id)
                .IsRequired()
                .UseIdentityColumn();
            builder.Property(p => p.FirstName).HasMaxLength(256);
            builder.Property(p => p.LastName).HasMaxLength(256);
            builder.Property(p => p.Email).HasMaxLength(50);
            builder.Property(p => p.PhoneNumber).HasMaxLength(50);
            builder.HasOne(e => e.Customer)
                .WithMany(e => e.Employees)                
                .HasForeignKey(e => e.CustomerNumber);
            builder.HasMany(e => e.BusinessLocations)
                .WithMany(e => e.Employees)
                .UsingEntity<EmployeeBusinessLocationEntity>(
                    l => l.HasOne<BusinessLocationEntity>().WithMany().HasForeignKey(e => e.BusinessLocationId),
                    l => l.HasOne<EmployeeEntity>().WithMany().HasForeignKey(e => e.EmployeeId).OnDelete(DeleteBehavior.NoAction)
                );
        }
    }
}

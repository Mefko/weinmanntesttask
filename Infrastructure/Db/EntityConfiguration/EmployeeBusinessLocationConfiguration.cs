﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public class EmployeeBusinessLocationConfiguration : BaseConfiguration<EmployeeBusinessLocationEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<EmployeeBusinessLocationEntity> builder)
        {
            builder.ToTable("EmployeeBusinessLocation");
            builder.HasKey(e => new { e.EmployeeId, e.BusinessLocationId });
        }
    }
}

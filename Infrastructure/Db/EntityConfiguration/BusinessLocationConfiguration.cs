﻿using Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Db.EntityConfiguration
{
    public class BusinessLocationConfiguration : BaseConfiguration<BusinessLocationEntity>
    {
        public override void ConfigureEntity(EntityTypeBuilder<BusinessLocationEntity> builder)
        {
            builder.ToTable("BusinessLocations");
            builder.HasKey(e => new { e.Id });
            builder.Property(p => p.Id)
                .IsRequired()
                .UseIdentityColumn();
            builder.Property(p=> p.Address).HasMaxLength(256);
            builder.Property(p => p.PhoneNumber).HasMaxLength(10);
            builder.HasOne(e => e.Customer)
                .WithMany(e => e.BusinessLocations)
                .HasForeignKey(e => e.CustomerNumber);
        }
    }
}

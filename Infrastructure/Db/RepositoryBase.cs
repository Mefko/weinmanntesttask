﻿using Domain.Contracts.Db;
using Domain.Exceptions;
using Domain.Models.Entities.Shared;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Db
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        private readonly DbContext _dbContext;

        protected DbContext DbContext { get { return _dbContext; } }
        protected DbSet<T> DbSet { get { return _dbContext.Set<T>(); } }

        protected RepositoryBase(ITestTaskDbContext dbContext)
        {
            _dbContext = dbContext as DbContext;
            if (_dbContext == null)
            {
                throw new ArgumentNullException($"{nameof(dbContext)} should be DbContext");
            }
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            DbSet.RemoveRange(entities);
        }

        public async Task<T> FindAsync(object key, CancellationToken cancellationToken = default)
        {
            var data = await DbSet.FindAsync(key, cancellationToken);
            return data;
        }

        public void Insert(T entity)
        {
            DbSet.Add(entity);
        }

        public void InsertRange(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities);
        }

        public async Task SaveAsync(Action<T> concurrencyHandler = null,CancellationToken cancellationToken = default)
        {
            try
            {
                await _dbContext.SaveChangesAsync(cancellationToken);
            } catch (DbUpdateConcurrencyException ex)
            {
                //TODO: here we can do our correct update if needed 
                if (concurrencyHandler == null)
                {   
                    throw new DbConcurrencyException(ex.Message);
                }
                foreach (var entry in ex.Entries)
                {
                    var currentValue = entry.Entity as T;
                    concurrencyHandler.Invoke(currentValue);
                }
            }
        }

        public async Task<IEnumerable<T>> SelectAllAsync(CancellationToken cancellationToken = default)
        {
            var data = await DbSet.ToArrayAsync(cancellationToken);
            return data;
        }

        public void Update(T entity)
        {
            var entry = _dbContext.Entry<T>(entity);

            if (entry?.State == EntityState.Detached)
            {
                _dbContext.Set<T>().Attach(entity);
                entry.State = EntityState.Modified;
            }
        }

        public void UpdateRange(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                Update(entity);
            }
        }
    }
}

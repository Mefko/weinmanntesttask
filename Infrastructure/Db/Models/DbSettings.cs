﻿namespace Infrastructure.Db.Models
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public int RetryCount { get; set; }
        public int MaxRetryTime { get; set; }
    }
}

﻿using AutoMapper;
using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Models;
using Domain.Models.Entities;

namespace Domain.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;
        private readonly IHashService _hashService;

        public CustomerService(ICustomerRepository customerRepository, 
            IMapper mapper,
            IHashService hashService)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
            _hashService = hashService;

        }
        public async Task<Customer> CreateAsync(Customer customer, string password)
        {
            var entity = _mapper.Map<CustomerEntity>(customer);
            entity.Password = _hashService.GetHash(password);
            _customerRepository.Insert(entity);
            await _customerRepository.SaveAsync();
            customer = _mapper.Map<Customer>(entity);
            return customer;
        }

        public async Task<bool> DeleteByNumberAsync(int number)
        {
            var customer = await _customerRepository.FindAsync(number);
            if(customer is null)
            {
                return false;
            }
            _customerRepository.Delete(customer);
            await _customerRepository.SaveAsync();
            return true;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            var entityCustomers = await _customerRepository.SelectAllAsync();
            var customers = _mapper.Map<IEnumerable<Customer>>(entityCustomers);
            return customers;
        }

        public async Task<Customer> GetByNumberAsync(int number)
        {
            var customerEntity = await _customerRepository.FindAsync(number);
            if(customerEntity == null)
            {
                return null;
            }
            var customer = _mapper.Map<Customer>(customerEntity);
            return customer;
        }

        public async Task<bool> IsPasswordValidAsync(int number, string pwd)
        {
            var entity = await _customerRepository.FindAsync(number);
            if (entity is null)
            {
                return false;
            }
            var pwdHash = _hashService.GetHash(pwd);
            return pwdHash == entity.Password;
        }

        public async Task<Customer> UpdateAsync(Customer customer)
        {
            var entity = await _customerRepository.FindAsync(customer.Number);
            if(entity is null)
            {
                return null;
            }
            _mapper.Map(customer, entity);
            await _customerRepository.SaveAsync();
            var updated = _mapper.Map<Customer>(entity);
            return updated;
        }
    }
}

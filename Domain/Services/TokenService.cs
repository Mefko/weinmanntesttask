﻿using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Models;

namespace Domain.Services
{
    public class TokenService : ITokenService
    {
        private readonly ITokenGenerator _tokenGenerator;
        private readonly ICustomerService _customerService;

        public TokenService(ITokenGenerator tokenGenerator,
            ICustomerService customerService)
        {
            _tokenGenerator = tokenGenerator;
            _customerService = customerService;
        }

        public async Task<Token> LoginAsync(CustomerAuth customerAuth)
        {
            var isPasswordValid = await _customerService
                .IsPasswordValidAsync(customerAuth.Number, customerAuth.Password);
            if (isPasswordValid)
            {
                var customer = await _customerService.GetByNumberAsync(customerAuth.Number);
                var token = await _tokenGenerator.GenerateAsync(customer, null);
                return token;
            }
            return null;
        }

        public Task<Token> RefreshTokenAsync(RefreshToken refreshToken)
        {
            throw new NotImplementedException();
        }
    }
}

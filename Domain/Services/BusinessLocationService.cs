﻿using AutoMapper;
using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Models;
using Domain.Models.Entities;

namespace Domain.Services
{
    public class BusinessLocationService : IBusinessLocationService
    {
        private readonly IMapper _mapper;
        private readonly IBusinessLocationRepository _businessLocationRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeBusinessLocationRepository _employeeBusinessLocationRepository;

        public BusinessLocationService(IMapper mapper,
            IBusinessLocationRepository businessLocationRepository,
            IEmployeeRepository employeeRepository,
            IEmployeeBusinessLocationRepository employeeBusinessLocationRepository)
        {
            _mapper = mapper;
            _businessLocationRepository = businessLocationRepository;
            _employeeBusinessLocationRepository = employeeBusinessLocationRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<BusinessLocation> CreateAsync(BusinessLocation businessLocation)
        {
            var entity = _mapper.Map<BusinessLocationEntity>(businessLocation);
            _businessLocationRepository.Insert(entity);
            await _businessLocationRepository.SaveAsync();
            businessLocation = _mapper.Map<BusinessLocation>(entity);
            return businessLocation;
        }

        public async Task<BusinessLocation> UpdateAsync(BusinessLocation businessLocation)
        {
            var entity = await _businessLocationRepository
                .GetByIdAndCustomerAsync(businessLocation.Id, businessLocation.CustomerNumber);
            if (entity is null)
            {
                return null;
            }
            _mapper.Map(businessLocation, entity);
            await _businessLocationRepository.SaveAsync();
            var updated = _mapper.Map<BusinessLocation>(entity);
            return updated;
        }

        public async Task<IEnumerable<BusinessLocation>> GetByCustomerAsync(int customerNumber)
        {
            var entities = await _businessLocationRepository.GetByCustomerAsync(customerNumber);
            var employees = _mapper.Map<IEnumerable<BusinessLocation>>(entities);
            return employees;
        }

        public async Task<bool> DeleteAsyc(int id, int customerNumber)
        {
            var entity = await _businessLocationRepository
                .GetByIdAndCustomerAsync(id, customerNumber);
            if (entity is null)
            {
                return false;
            }
            _businessLocationRepository.Delete(entity);
            await _businessLocationRepository.SaveAsync();
            return true;
        }

        public async Task<IEnumerable<BusinessLocationEmployee>> GetLocationWithEmployeeByCustomerAsync(int customerNumber)
        {
            var entities = await _businessLocationRepository.GetByCustomerAsync(customerNumber, true);
            var employees = _mapper.Map<IEnumerable<BusinessLocationEmployee>>(entities);
            return employees;
        }

        public async Task<BusinessLocationEmployee> AddEmployeeToLocation(int locationId,
            int employeeId, int customerNumber)
        {
            //TODO: need to do in better way
            var employeeEntity = await _employeeRepository.FindAsync(employeeId);
            if (employeeEntity == null || employeeEntity.CustomerNumber != customerNumber)
            {
                throw new ArgumentException("Can't find employee");
            }
            var locationEntity = await _businessLocationRepository.FindAsync(locationId);
            if (locationEntity == null || locationEntity.CustomerNumber != customerNumber)
            {
                throw new ArgumentException("Can't find Business Location");
            }
            var ls = new EmployeeBusinessLocationEntity()
            {
                EmployeeId = employeeId,
                BusinessLocationId = locationId
            };
            _employeeBusinessLocationRepository.Insert(ls);
            await _employeeBusinessLocationRepository.SaveAsync();
            var locationWithEmployeeEntity = await _businessLocationRepository
                .GetByIdAndCustomerAsync(locationId, customerNumber, true);
            var locationWithEmployee = _mapper
                .Map<BusinessLocationEmployee>(locationWithEmployeeEntity);
            return locationWithEmployee;
        }

        public async Task<BusinessLocationEmployee> RemoveEmployeeToLocation(int locationId, int employeeId, int customerNumber)
        {
            //TODO: can be better
            var locationEntity = await _businessLocationRepository.FindAsync(locationId);
            if (locationEntity == null || locationEntity.CustomerNumber != customerNumber)
            {
                throw new ArgumentException("Can't find Business Location");
            }

            var employeeEntity = await _employeeRepository.FindAsync(employeeId);
            if (employeeEntity == null || employeeEntity.CustomerNumber != customerNumber)
            {
                throw new ArgumentException("Can't find employee");
            }

            var ebEntity = await _employeeBusinessLocationRepository
                .GetByCustomerIdAsync(locationId, employeeId);
            if (ebEntity != null)
            {
                _employeeBusinessLocationRepository.Delete(ebEntity);
                await _employeeBusinessLocationRepository.SaveAsync();
                locationEntity = await _businessLocationRepository.FindAsync(locationId);
            }

            var locationWithEmployee = _mapper
                .Map<BusinessLocationEmployee>(locationEntity);
            return locationWithEmployee;
        }
    }
}

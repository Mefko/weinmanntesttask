﻿using AutoMapper;
using Domain.Contracts.Db.Repositories;
using Domain.Contracts.Services;
using Domain.Models;
using Domain.Models.Entities;

namespace Domain.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;
        public EmployeeService(IEmployeeRepository employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }
        public async Task<Employee> CreateAsync(Employee employee)
        {
            var entity = _mapper.Map<EmployeeEntity>(employee);
            _employeeRepository.Insert(entity);
            await _employeeRepository.SaveAsync();
            employee = _mapper.Map<Employee>(entity);
            return employee;
        }

        public async Task<bool> DeleteAsyc(int id, int customerNumber)
        {
            var entity = await _employeeRepository
                .GetByIdAndCustomerAsync(id, customerNumber);
            if (entity is null)
            {
                return false;
            }
            _employeeRepository.Delete(entity);
            await _employeeRepository.SaveAsync();
            return true;
        }

        public async Task<IEnumerable<Employee>> GetByCustomerAsync(int customerNumber)
        {
            var entities = await _employeeRepository.GetByCustomerAsync(customerNumber);
            var employees = _mapper.Map<IEnumerable<Employee>>(entities);
            return employees;
        }

        public async Task<Employee> UpdateAsyc(Employee employee)
        {
            var entity = await _employeeRepository
                .GetByIdAndCustomerAsync(employee.Id, employee.CustomerNumber);
            if (entity is null)
            {
                return null;
            }
            _mapper.Map(employee, entity);
            await _employeeRepository.SaveAsync();
            var updated = _mapper.Map<Employee>(entity);
            return updated;
        }
    }
}

﻿namespace Domain.Models
{
    public class CustomerAuth
    {
        public int Number { get; set; }

        public string Password { get; set; }
    }
}

﻿namespace Domain.Models
{
    public class BusinessLocationEmployee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int CustomerNumber { get; set; }
        public IList<Employee> Employees { get; set; }
    }
}

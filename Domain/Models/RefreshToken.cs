﻿namespace Domain.Models
{
    public class RefreshToken
    {
        public string Token { get; set; }
        public string RefreshTokenValue { get; set; }
    }
}

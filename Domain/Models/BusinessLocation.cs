﻿namespace Domain.Models
{
    public class BusinessLocation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int CustomerNumber { get; set; }
    }
}

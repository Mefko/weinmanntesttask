﻿namespace Domain.Models
{
    public class Token
    {
        public string TokenValue { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}

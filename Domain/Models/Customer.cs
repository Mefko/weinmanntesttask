﻿namespace Domain.Models
{
    public class Customer
    {
        public int Number { get; set; }
        public string Name { get; set; }
    }
}

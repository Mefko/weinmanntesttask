﻿using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class RoleEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<CustomerEntity> Customers { get; set; }
    }
}

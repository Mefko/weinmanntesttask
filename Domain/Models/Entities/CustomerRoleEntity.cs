﻿using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class CustomerRoleEntity : EntityBase
    {
        public int CustomerId { get; set; }
        public int RoleId {  get; set; }
        public CustomerEntity Customer { get; set; }
        public RoleEntity Role { get; set; }

    }
}

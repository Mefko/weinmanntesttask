﻿namespace Domain.Models.Entities.Shared
{
    public class EntityBase
    {
        public byte[] Version { get; set; }
    }
}

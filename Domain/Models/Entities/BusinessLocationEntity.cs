﻿using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class BusinessLocationEntity : EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int CustomerNumber { get; set; }
        public CustomerEntity Customer { get; set; }
        public IList<EmployeeEntity> Employees { get; set; }
    }
}

﻿using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class EmployeeEntity : EntityBase
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int CustomerNumber { get; set; }
        public CustomerEntity Customer { get; set; }
        public IList<BusinessLocationEntity> BusinessLocations { get; set; }
    }
}

﻿
using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class CustomerEntity : EntityBase
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public IList<RoleEntity> Roles { get; set; }
        public IList<BusinessLocationEntity> BusinessLocations { get; set; }
        public IList<EmployeeEntity> Employees { get; set; }
    }
}

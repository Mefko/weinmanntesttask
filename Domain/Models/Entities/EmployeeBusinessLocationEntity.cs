﻿using Domain.Models.Entities.Shared;

namespace Domain.Models.Entities
{
    public class EmployeeBusinessLocationEntity : EntityBase
    {
        public int EmployeeId { get; set; }
        public int BusinessLocationId { get; set; }
    }
}

﻿using Domain.Models;

namespace Domain.Contracts.Services
{
    public interface ITokenService
    {
        Task<Token> LoginAsync(CustomerAuth customerAuth);
        Task<Token> RefreshTokenAsync(RefreshToken refreshToken);
    }
}

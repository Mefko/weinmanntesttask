﻿namespace Domain.Contracts.Services
{
    public interface IHashService
    {
        string GetHash(string value);
    }
}

﻿using Domain.Models;

namespace Domain.Contracts.Services
{
    public interface ITokenGenerator
    {
        Task<Token> GenerateAsync(Customer customer, RefreshToken refreshToken);

        Task<Token> RegenerateAsync(RefreshToken refreshToken);
    }
}

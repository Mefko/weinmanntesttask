﻿using Domain.Models;

namespace Domain.Contracts.Services
{
    public interface IBusinessLocationService
    {
        Task<IEnumerable<BusinessLocation>> GetByCustomerAsync(int customerNumber);
        Task<BusinessLocation> CreateAsync(BusinessLocation businessLocation);
        Task<BusinessLocation> UpdateAsync(BusinessLocation businessLocation);
        Task<bool> DeleteAsyc(int id, int customerNumber);

        Task<IEnumerable<BusinessLocationEmployee>> 
            GetLocationWithEmployeeByCustomerAsync(int customerNumber);
        Task<BusinessLocationEmployee> AddEmployeeToLocation(int locationId, int employee, int customerNumber);
        Task<BusinessLocationEmployee> RemoveEmployeeToLocation(int locationId, int employee, int customerNumber);
    }
}

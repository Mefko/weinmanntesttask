﻿using Domain.Models;

namespace Domain.Contracts.Services
{
    public interface ICustomerService
    {
        public Task<IEnumerable<Customer>> GetAllAsync();
        public Task<Customer> CreateAsync(Customer customer, string password);

        public Task<Customer> GetByNumberAsync(int number);

        public Task<Customer> UpdateAsync(Customer customer);

        public Task<bool> DeleteByNumberAsync(int number);

        public Task<bool> IsPasswordValidAsync(int number, string pwd);
    }
}

﻿using Domain.Models;

namespace Domain.Contracts.Services
{
    public interface IEmployeeService
    {
        Task<IEnumerable<Employee>> GetByCustomerAsync(int customerNumber);
        Task<Employee> CreateAsync(Employee employee);
        Task<Employee> UpdateAsyc(Employee employee);
        Task<bool> DeleteAsyc(int id, int customerNumber);
    }
}

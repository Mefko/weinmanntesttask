﻿namespace Domain.Contracts.Db
{
    public interface ITestTaskDbContext
    {
        bool IsAlive();
        void Migrate();
    }
}

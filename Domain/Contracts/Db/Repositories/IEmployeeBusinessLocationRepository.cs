﻿using Domain.Models.Entities;

namespace Domain.Contracts.Db.Repositories
{
    public interface IEmployeeBusinessLocationRepository : 
        IRepositoryBase<EmployeeBusinessLocationEntity>
    {
        Task<EmployeeBusinessLocationEntity> GetByCustomerIdAsync(int locationId, int employeeId);
    }
}

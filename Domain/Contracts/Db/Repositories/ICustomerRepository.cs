﻿using Domain.Models.Entities;

namespace Domain.Contracts.Db.Repositories
{
    public interface ICustomerRepository : IRepositoryBase<CustomerEntity>
    {
    }
}

﻿using Domain.Models.Entities;

namespace Domain.Contracts.Db.Repositories
{
    public interface IEmployeeRepository : IRepositoryBase<EmployeeEntity>
    {
        Task<IEnumerable<EmployeeEntity>> GetByCustomerAsync(int customerNumber);
        Task<EmployeeEntity> GetByIdAndCustomerAsync(int id, int customerNumber);
    }
}

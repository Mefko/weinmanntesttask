﻿using Domain.Models.Entities;

namespace Domain.Contracts.Db.Repositories
{
    public interface ICustomerRolesRepository : IRepositoryBase<CustomerRoleEntity>
    {
        public Task<IEnumerable<RoleEntity>> GetRolesByCustomerAsync(int customerNumber);
    }
}

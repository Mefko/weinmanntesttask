﻿using Domain.Models.Entities;

namespace Domain.Contracts.Db.Repositories
{
    public interface IBusinessLocationRepository : IRepositoryBase<BusinessLocationEntity>
    {
        Task<IEnumerable<BusinessLocationEntity>> GetByCustomerAsync
            (int customerNumber, bool includeEmployees = false);
        Task<BusinessLocationEntity> GetByIdAndCustomerAsync
            (int id, int customerNumber, bool includeEmployees = false);
    }
}

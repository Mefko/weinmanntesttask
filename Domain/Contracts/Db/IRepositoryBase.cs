﻿using Domain.Models.Entities.Shared;

namespace Domain.Contracts.Db
{
    public interface IRepositoryBase<T> where T : EntityBase
    {
        void Insert(T entity);

        void InsertRange(IEnumerable<T> entities);

        void Update(T entity);

        void UpdateRange(IEnumerable<T> entities);

        void Delete(T entity);

        void DeleteRange(IEnumerable<T> entities);

        Task<T> FindAsync(object key, CancellationToken cancellationToken = default);

        Task<IEnumerable<T>> SelectAllAsync(CancellationToken cancellationToken = default);

        Task SaveAsync(Action<T> concurrencyHandler = null, CancellationToken cancellationToken = default);
    }
}
